﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
public class GameManager : MonoBehaviour {

	public MyPathNode[,] grid;
	public GameObject enemy;
	public GameObject gridBox;
	public int gridWidth;
	public int gridHeight;

	public float gridSize;
	public GUIStyle lblStyle;

	public static string distanceType;

	public List<int[]> trapList;
	
	public static int distance = 1;

	// Sorting gameObjects
	GameObject activeMenu;
	GameObject builds;

	// Player fail / enemy success counter
	GameObject failCounter;
	int failCount;
	int playerHealth;

	// Player Money
	public int playerMoney;
	GameObject moneyCounter;
	GameObject restartScreen;

	// Enemy Waves
	int lastClearedWave;
	int currentWave;
	int enemiesLeft;
	int enemiesSpawned;
	bool spawning;

	List<GameObject> enemiesOnScreen;
	List<GameObject> visualGrid;
	bool showingGrid;

	public float defaultScreenWidth;
	public float defaultUIFontSize;
	public float defaultBuildFontSize;
	public float defaultTitleSize;

	public bool gameOver;

	public class BuildCosts {
		public int Turret;
		public int Trap;
		public int Wall;

		// Turret upgrades
		public int Cannon;
		public int Tesla;
		public int Laser;
	}

	public BuildCosts buildCosts;

	void Awake () {
		defaultScreenWidth = 1024;

		SetupGameUI();

		trapList = new List<int[]>();

		SetBuildCosts();
	
		//Generate a grid - nodes according to the specified size
		grid = new MyPathNode[gridWidth, gridHeight];

		for (int x = 0; x < gridWidth; x++) {
			for (int y = 0; y < gridHeight; y++) {
				//Boolean isWall = ((y % 2) != 0) && (rnd.Next (0, 10) != 8);
				Boolean isWall = false;
				grid [x, y] = new MyPathNode ()
				{
					IsWall = isWall,
					X = x,
					Y = y,
				};
			}
		}

		builds = new GameObject("Player Buildings");

		//instantiate grid gameobjects to display on the scene
		CreateGrid ();

		currentWave = 1;

		enemiesOnScreen = new List<GameObject>();
	}

	void SetupGameUI () {
		defaultUIFontSize = 24;
		defaultBuildFontSize = 16;
		defaultTitleSize = 48;

		int UIFontSize = ScaleUI(defaultUIFontSize);

		// Fail Counter
		failCount = 0;
		playerHealth = 10;
		failCounter = GameObject.Find("UI").transform.FindChild ("FailCounter").gameObject;
		failCounter.GetComponent<Text>().fontSize = UIFontSize;

		// Money Counter
		playerMoney = 1000;
		moneyCounter = GameObject.Find("UI").transform.FindChild ("Money").gameObject;

		moneyCounter.GetComponent<Text>().fontSize = UIFontSize;

		restartScreen = GameObject.Find ("UI").transform.FindChild ("Restart").gameObject;
		Text loseTitle = restartScreen.transform.FindChild ("Lost").GetComponent<Text>();
		Text progressText = restartScreen.transform.FindChild ("Progress").GetComponent<Text>();

		loseTitle.fontSize = ScaleUI(defaultTitleSize);
		loseTitle.fontStyle = FontStyle.Bold;

		progressText.fontSize = UIFontSize;
		restartScreen.SetActive(false);
	}

	public int ScaleUI(float sizeToScale) {
		return Mathf.RoundToInt (sizeToScale * Screen.width / defaultScreenWidth);
	}

	void SetBuildCosts () {
		buildCosts = new BuildCosts();
		buildCosts.Turret = 200;
		buildCosts.Wall = 50;
		buildCosts.Trap = 100;

		buildCosts.Cannon = 300;
		buildCosts.Laser = 200;
		buildCosts.Tesla = 250;
	}

	public void SetActiveMenu (GameObject newMenu) {
		if (activeMenu) {
			GameObject.Destroy(activeMenu);
		}
		activeMenu = newMenu;
	}

	public void OnEnemySuccess () {
		failCount++;
		if (failCount == playerHealth) {
			GameOver();
		}
	}

	public bool CanSpendMoney (int amount) {
		if (playerMoney < amount) {
			return false;
		} else {
			return true;
		}
	}

	public void SpendMoney (int amount) {
		playerMoney -= amount;
	}

	public void EarnMoney (int amount) {
		playerMoney += amount;
	}

	void GameOver () {
		for (int i=0; i<enemiesOnScreen.Count; i++) {
			Destroy (enemiesOnScreen[i]);
		}
		string progressText = "You made it to wave " + lastClearedWave.ToString () + "!";
		restartScreen.transform.FindChild ("Progress").GetComponent<Text>().text = progressText;
		restartScreen.SetActive(true);
	}

	public void RestartGame() {
		Application.LoadLevel(Application.loadedLevel);
	}

	void OnGUI()
	{
		if (!spawning) {
			if(GUI.Button(new Rect(Screen.width/2 - 300,Screen.height - 100f,200f,50f),"Launch Wave"))
			{
				StartCoroutine("SetupWave");
				SetActiveMenu (null);
			}
		} else {
			GUI.Button(new Rect(Screen.width/2 - 300,Screen.height - 100f,200f,50f),"Wave Launching...");
		}
		if(GUI.Button(new Rect(Screen.width/2 + 50,Screen.height - 100f,200f,50f),"Reload"))
		{
			Application.LoadLevel(Application.loadedLevel);
		}

		failCounter.GetComponent<Text>().text = failCount.ToString () + " / " + playerHealth.ToString();
		moneyCounter.GetComponent<Text>().text = "$$: " + (playerMoney).ToString ();

		/*GUI.Label(new Rect(5f,120f,200f,200f),"Click on the grid to place a wall/tower.\nYou can change the distance formula of the path to Euclidean, " +
			"Manhattan etc\nYou can also change the Grid size in the GameManager variables from the inspector",lblStyle);*/
	}


	void CreateGrid()
	{
		showingGrid = true;
		visualGrid = new List<GameObject>();
		//Generate gameobjects of gridBoxes to show on the screen
		for (int i =0; i<gridHeight; i++) {
			for (int j =0; j<gridWidth; j++) {
				GameObject nobj = (GameObject)GameObject.Instantiate(gridBox);
				nobj.transform.position = new Vector2(gridBox.transform.position.x + (gridSize*j), gridBox.transform.position.y + (0.87f*i));
				nobj.name = j+","+i;

				nobj.gameObject.transform.parent = gridBox.transform.parent;
				nobj.SetActive(true);
				visualGrid.Add(nobj);
			}
		}
		SetupEntryExit();
	}

	void SetupEntryExit() {
		Transform map = gridBox.transform.parent; 

		// This is lazy and I need a better way of generating this from the enemy script
		GameObject[] noBuildZone = new GameObject[]
		{	
			map.FindChild ("0,6").gameObject, 
			map.FindChild ("0,5").gameObject,
			map.FindChild ((gridWidth-1).ToString () + ",6").gameObject, 
			map.FindChild ((gridWidth-1).ToString () + ",5").gameObject
		};

		for (int i=0; i<noBuildZone.Length; i++) {
			noBuildZone[i].GetComponent<SpriteRenderer>().material.color = Color.red;
			noBuildZone[i].GetComponent<RadialMenu>().canBuild = false;
		}
	}

	// Hurray coroutines!
	IEnumerator SetupWave () {
		spawning = true;
		enemiesSpawned = ((currentWave + 1) / 2) + 10;
		enemiesLeft = enemiesSpawned;

		int enemyHealth = ((currentWave + 1) / 2) * 7;
		int enemySpeed = currentWave / 10 + 2;

		for (int i=0; i<enemiesSpawned; i++) {
			createEnemy(enemyHealth, enemySpeed, i);
			yield return new WaitForSeconds(.5f);
		}
		currentWave++;

		gameObject.GetComponent<EnemyAStar>().SetMasterPath (grid);

		spawning = false;
	}

	void createEnemy(int health, int speed, int enemyNumber)
	{
		GameObject nb = (GameObject)Instantiate (enemy, new Vector3(-17, 1), Quaternion.Euler(0, 0, 270));
		nb.name = "Enemy:" + currentWave + ":" +  enemyNumber;
		nb.GetComponent<EnemyStats>().SetHealth(health);
		nb.GetComponent<EnemyAStar>().moveSpeed = (float)speed;
		nb.SetActive (true);

		enemiesOnScreen.Add (nb);
	}

	public void OnEnemyDeath (GameObject deadEnemy) {
		foreach (GameObject enemy in enemiesOnScreen) {
			if (deadEnemy.name == enemy.name) {
				enemiesOnScreen.Remove (enemy);
				break;
			}
		}
	}

	// Ensures that there is still a viable path before building a structure
	// Takes in coordinates of grid space about to be blocked
	public bool CheckForPath (int x, int y) {
		// Block this space temporarily
		grid [x, y].IsWall = true;

		// Then check to see if a path exists
		bool path = gameObject.GetComponent<EnemyAStar>().CheckForPath();
		
		// Since this is not an actual build, revert the grid to previous state
		grid [x, y].IsWall = false;

		return path;
	}

	// Update is called once per frame
	void Update () {
		if (enemiesOnScreen.Count > 0 && showingGrid) {
			HideGrid ();
			lastClearedWave = currentWave;
		} else if (enemiesOnScreen.Count == 0 && !showingGrid){
			ShowGrid ();
		}
	}

	void HideGrid () {
		for (int i=0; i<visualGrid.Count; i++) {
			visualGrid[i].SetActive(false);
		}
		showingGrid = false;
	}

	void ShowGrid () {
		for (int i=0; i<visualGrid.Count; i++) {
			visualGrid[i].SetActive(true);
		}
		showingGrid = true;
	}
	
	public void addWall (int x, int y)
	{
		grid [x, y].IsWall = true;
	}
	
	public void removeWall (int x, int y)
	{
		grid [x, y].IsWall = false;
	}

}
