﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turret : MonoBehaviour {
	string turretType;

	GameObject target;
	int range;

	GridPosition turretPos;
	List<GridPosition> fireZone;

	public int cost;

	public GameObject Bullet;

	bool inRange;

	bool firing;
	float fireTimer;

	public Stats stats;

	public class Stats{
		public float bulletSpeed;
		public float fireRate;
		public int damage;

		public int upgradeLevel;

		public bool splash;
		public int splashSize;
		public int splashDecay;

		public bool chain;
		public int chainSize;
		public int chainDecay;

		public Stats () {
			this.fireRate = .5f;
			this.damage = 10;
			this.bulletSpeed = .4f;

			this.upgradeLevel = 1;
			this.splash = false;
			this.chain = false;
		}
	};

	public class GridPosition{
		public int x =0;
		public int y=0;
		
		public GridPosition()
		{
		}
		
		public GridPosition (int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	};

	// Use this for initialization
	void Awake () {
		fireTimer = 0;
		range = 3;

		stats = new Stats();

		InvokeRepeating ("FindClosestEnemy", 0, 1);
		InvokeRepeating ("GetTargetPosition", 0, .3f);
	}

	// Update is called once per frame
	void Update () {
		if (target && inRange) {
			FaceTarget (target.transform.position);
			Shoot (target);
		} else {
			IdlePosition();
		}
	}

	void GetTargetPosition () {
		if (target) {
			EnemyAStar enemy = target.GetComponent<EnemyAStar>();
			GridPosition targetGridPos = new GridPosition(enemy.currentGridPosition.x, enemy.currentGridPosition.y);
			bool match = false;
			for (int i = 0; i < fireZone.Count; i++) {
				if (targetGridPos.x == fireZone[i].x && targetGridPos.y == fireZone[i].y) {
					inRange = true;
					match = true;
				}
			}
			if (!match) {
				inRange = false;
			}
		}
	}

	void Shoot (GameObject enemy) {
		if (CanShoot()) {
			Vector3 bulletSpawn = transform.FindChild("Pivot").FindChild ("BulletSpawn").position;
			Quaternion bulletRot = transform.FindChild ("Pivot").rotation;
			GameObject bullet = (GameObject)Instantiate(Bullet, bulletSpawn, bulletRot);
			Bullet bulletScript = bullet.GetComponent<Bullet>();
			bulletScript.damage = stats.damage;
			bulletScript.speed = stats.bulletSpeed;
			bulletScript.target = target;
		}
	}

	bool CanShoot () {
		// If this is the first time we've tried to shoot, we can shoot
		if (!firing) {
			firing = true;
			return true;
		} else {
			// Else, figure out when the last time we shot was
			fireTimer += Time.deltaTime;
			// if the total time between our last shot and now is greater than the fire rate, we can shoot
			if (fireTimer > stats.fireRate) {
				fireTimer = 0;
				return true;
			} else {
				return false;
			}
		}
	}

	public void AddGridPosition (string[] gridPos) {
		turretPos = new GridPosition(int.Parse(gridPos[0]), int.Parse (gridPos[1]));

		fireZone = new List<GridPosition>();

		BuildFireZone (turretPos, range, "left");
		BuildFireZone (new GridPosition(turretPos.x+1, turretPos.y), range-1, "right");

		/*Debug.Log ("Turret Position -----------");
		Debug.Log (turretPos.x);
		Debug.Log (turretPos.y);*/

		for (int i=0; i<fireZone.Count; i++) {
			/*Debug.Log ("Grid Position ----------");
			Debug.Log (fireZone[i].x);
			Debug.Log (fireZone[i].y);*/
		}
	}
	
	void BuildFireZone(GridPosition position, int range, string direction) {
		// Always adds current position
		if (position != turretPos) {
			fireZone.Add(position);
		}

		// End the recursive function if range = 0
		if (range <= 0) {
			return;
		}

		// Grab all cells within vertical range
		for (int i=1; i <= range; i++) {
			fireZone.Add (new GridPosition(position.x, position.y + i));
			fireZone.Add (new GridPosition(position.x, position.y - i));
		}

		// Get new position in given direction
		GridPosition newPos;
		if (direction == "right") {
			newPos = new GridPosition(position.x + 1, position.y);
		} else {
			newPos = new GridPosition(position.x - 1, position.y);
		}

		// Recursively call function with new position
		BuildFireZone (newPos, range-1, direction);
	}

	void FindClosestEnemy() {
		// Find all enemies
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

		for (int i=0; i<enemies.Length; i++) {
			if (!enemies[i].GetComponent<EnemyAStar>().onGrid) {
				continue;
			}
			// Set target if none
			if (!target) {
				target = enemies[i];
			} else {
				// newDistance = current iterators distance to turret
				float newDistance = Vector3.Distance(enemies[i].transform.position, transform.position);
				// closestDistance = target's distance to turret 
				float targetDistance = Vector3.Distance(target.transform.position, transform.position);
				// Set new closest if its less than current closest
				if (newDistance < targetDistance) {
					target = enemies[i];
				}
			}
		}
	}

	void IdlePosition () {
		GameObject pivot = transform.FindChild ("Pivot").gameObject;
		pivot.transform.rotation = Quaternion.Slerp (pivot.transform.rotation, Quaternion.identity, Time.deltaTime * 12);
	}

	void FaceTarget (Vector3 targetPos) {
		GameObject pivot = transform.FindChild ("Pivot").gameObject;


		// Configures a new rotation based on the target's transform position
		Quaternion newRotation = Quaternion.LookRotation(pivot.transform.position - targetPos, Vector3.back);
		//newRotation *= Quaternion.Euler (0, 0, 180);
		
		// Keeps the look rotation limited to the Z access (since we're in 2D)
		newRotation.x = 0;
		newRotation.y = 0;
		
		// "Slerps" the rotation so the rotation change isn't immediate.
		pivot.transform.rotation = Quaternion.Slerp (pivot.transform.rotation, newRotation, Time.deltaTime * 20);
	}
}
