﻿using UnityEngine;
using System.Collections;

public class RadialMenu : MonoBehaviour {

	public GameObject Button;

	GameManager gameManager;

	string[] buttonTypes;

	int ringCount;
	float radius;
	float angle;

	int gridX;
	int gridY;

	// Controls whether this space can be built upon or not (not the same as already having a building)
	public bool canBuild;

	GameObject menu;

	PlayerBuilding building;

	// Use this for initialization
	void Awake () {
		canBuild = true;
		radius = 1;

		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		string[] grid = gameObject.name.Split (',');
		gridX = int.Parse(grid[0]);
		gridY = int.Parse(grid[1]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown () {
		gameManager.SetActiveMenu(null);
		// If you can't build, or if its game over, do nothing
		if (!canBuild || gameManager.gameOver) {
			return;
		}
		// If a menu doesn't exist, create one
		if (!menu) {
			InitializeMenu ();
		} else {
		// Otherwise, destroy the existing menu
			Destroy (menu);
		}
	}

	void InitializeMenu () {
		menu = new GameObject("Radial Menu");
		menu.transform.position = new Vector3(transform.position.x, transform.position.y, -1);
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		gameManager.SetActiveMenu(menu);

		string buildType = GetBuildType ();

		// If no building is present
		switch(buildType) {
		case "Turret":
		case "Trap":
			ringCount = 2;
			buttonTypes = new string[] {/*"Tesla", "Cannon",*/"Upgrade", "Salvage"/*, "Laser"*/};
			break;
		case "Wall":
			ringCount = 1;
			buttonTypes = new string[] {/*"Tesla", "Cannon",*/"Salvage"/*, "Laser"*/};
			return;
		default:
			ringCount = 3;
			buttonTypes = new string[] {"Turret", "Wall", "Trap"};
			break;
		}

		GameObject[] buttons = new GameObject[ringCount];

		for (int i=0; i<ringCount; i++) {
			// Crazy math stuff (Thank you Katie)
			float newX = transform.position.x + radius * Mathf.Cos(2 * Mathf.PI * i / ringCount + Mathf.PI /  2);
			float newY = transform.position.y + radius * Mathf.Sin(2 * Mathf.PI * i / ringCount + Mathf.PI / 2);

			Vector3 position = new Vector3(newX, newY, -1);

			// Instantiate button and pass necessary params to the button script.
			GameObject button = buttons[i] = ((GameObject)Instantiate (Button, position, Quaternion.identity));
			button.transform.parent = menu.transform;
			button.transform.FindChild ("icon").GetComponent<SpriteRenderer>().sprite = LoadButtonSprite (buttonTypes[i]);
			Button buttonScript = button.GetComponent<Button>();
			buttonScript.buttonType = buttonTypes[i];
			buttonScript.gridPos = gameObject;
			buttonScript.parentMenu = this;

			if (buttonTypes[i] == "Upgrade" || buttonTypes[i] == "Salvage") {
				buttonScript.building = building.gameObject;
			}
			buttonScript.InitializeCost();

			// These two if statements do the same thing but for different reasons.
			// If you can't afford this building
			if (!gameManager.CanSpendMoney(buttonScript.cost)) {
				button.transform.FindChild("icon").GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, .3f);
				buttonScript.disabled = true;
			}
			// Or if there is no viable path when this spot is blocked
			if (buttonTypes[i] != "Trap" && !gameManager.CheckForPath (gridX, gridY)) {
				button.transform.FindChild("icon").GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, .3f);
				buttonScript.disabled = true;
			}
		}
	}

	string GetBuildType () {
		PlayerBuilding[] buildings = GameObject.Find ("Player Buildings").transform.GetComponentsInChildren<PlayerBuilding>();
		for (int i=0; i<buildings.Length; i++) {
			string[] split = buildings[i].gameObject.name.Split('-');
			if (split.Length > 1 && gameObject.name == split[1]) {
				building = buildings[i];
				return split[0];
			}
		}
		return null;
	}

	Sprite LoadButtonSprite (string buttonType) {
		switch (buttonType) {
		case "Turret":
			return Resources.Load<Sprite> ("Buttons/turret") as Sprite;
		case "Wall":
			return Resources.Load<Sprite> ("Buttons/walls") as Sprite;
		case "Trap":
			return Resources.Load<Sprite> ("Buttons/traps") as Sprite;
		case "Cannon":
			return Resources.Load<Sprite> ("Buttons/fireball-turret") as Sprite;
		case "Laser":
			return Resources.Load<Sprite> ("Buttons/tesla-turret") as Sprite;
		case "Tesla":
			return Resources.Load<Sprite> ("Buttons/lightning-arc") as Sprite;
		case "Salvage":
			return Resources.Load<Sprite> ("Buttons/wrecking-ball") as Sprite;
		case "Upgrade":
			return Resources.Load<Sprite> ("Buttons/upgrade") as Sprite;
		case "Close":
		default:
			return Resources.Load<Sprite> ("Buttons/cancel") as Sprite;
		}
	}

	// Sets whether this space can be built upon or not (not the same as already having a building)
	public void SetBuildStatus (bool status) {

	}
}
