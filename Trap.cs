﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {

	public class GridPosition{
		public int x =0;
		public int y=0;
		
		public GridPosition()
		{
		}
		
		public GridPosition (int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	};

	public float slowFactor;

	GridPosition turretPos;

	GameObject[] targets;

	// Use this for initialization
	void Start () {
		// Percentage speed that an enemy should move while on top of trap
		slowFactor = .75f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddGridPosition (string[] gridPos) {
		int parsedX = int.Parse(gridPos[0]);
		int parsedY = int.Parse(gridPos[1]);

		turretPos = new GridPosition(parsedX, parsedY);
		GameObject.Find ("GameManager").GetComponent<GameManager>().trapList.Add (new int[]{parsedX, parsedY});
	}
}
