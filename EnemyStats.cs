﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour {
	int health;
	int healthTotal;
	int bounty;

	GameManager gameManager;
	GameObject healthBar;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		//healthBar = transform.FindChild ("HealthBar").FindChild("Health").gameObject; 
	}

	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			gameManager.EarnMoney (bounty);
			gameManager.OnEnemyDeath (gameObject);
			Destroy (gameObject);
		}
		float healthPerc = health / healthTotal;
		//healthBar.transform.localScale = new Vector3(transform.localScale.x, healthPerc, transform.localScale.z);
		//healthBar.transform.localPosition = new Vector3(transform.localPosition.x, 1 - healthPerc, transform.localPosition.z);
	}

	public void SetHealth (int totalHealth) {
		health = totalHealth;
		healthTotal = totalHealth;
	}

	public void DeductHealth (int damage) {
		health = health - damage;
	}
}
