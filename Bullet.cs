﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed;
	public int damage;

	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}

	void OnColliderEnter2D() {

	}

	// Update is called once per frame
	void Update () {
		if (target) {
			if (Vector3.Distance(transform.position, target.transform.position) < .5f) {
				target.GetComponent<EnemyStats>().DeductHealth(damage);
				Destroy (gameObject);
			}
			if (target) {
				transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed);
			}
		} else {
			Destroy(gameObject);
		}
	}
}
