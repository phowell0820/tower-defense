﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	// Prefabs
	public GameObject Turret;
	public GameObject Cannon;
	public GameObject Tesla;
	public GameObject Laser;
	public GameObject Trap;
	public GameObject Wall;

	// Type of button (handles button behavior)
	public string buttonType;
	// The grid object that corresponds to this button
	public GameObject gridPos;
	// This button's parent radial menu
	public RadialMenu parentMenu;
	// The player building this button corresponds to.
	public GameObject building;

	int salvageAmount;

	GameManager gameManager;
	EnemyAStar masterPathfinder;

	GameObject buttonText;

	public int cost;
	public bool disabled;

	// Use this for initialization
	void Awake () {
		disabled = false;
		GameObject game = GameObject.Find ("GameManager");
		gameManager = game.GetComponent<GameManager>();

		masterPathfinder = game.GetComponent<EnemyAStar>();

		if (buttonType == "Turret") {
			transform.FindChild ("icon").transform.localPosition = new Vector3 (0.6f, 0, 0);
		}
	}

	public void InitializeCost () {
		switch(buttonType) {
		case "Turret":
			cost = gameManager.buildCosts.Turret;
			break;
		case "Trap":
			cost = gameManager.buildCosts.Trap;
			break;
		case "Wall":
			cost = gameManager.buildCosts.Wall;
			break;
		case "Upgrade":
			cost = Mathf.RoundToInt(building.GetComponent<PlayerBuilding>().lastCost * 1.2f);
			break;
		case "Salvage":
			salvageAmount = building.GetComponent<PlayerBuilding>().totalCost / 2;
			break;
		default:
			Debug.Log ("Invalid Button Type");
			break;
		}

		buttonText = new GameObject();
		buttonText.transform.parent = transform;
		buttonText.name = "Cost";

		GUIText costDisplay = buttonText.AddComponent<GUIText>();
		if (buttonType == "Salvage") {
			costDisplay.text = salvageAmount.ToString ();
			costDisplay.color = new Color(0, .5f, 0);
		} else {
			costDisplay.text = cost.ToString ();
			costDisplay.color = new Color(.5f, 0, 0);
		}
		costDisplay.fontSize = Mathf.RoundToInt (gameManager.defaultBuildFontSize * Screen.width / gameManager.defaultScreenWidth);

		costDisplay.fontStyle = FontStyle.Bold;
		costDisplay.alignment = TextAlignment.Center;
		costDisplay.anchor = TextAnchor.UpperCenter;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 adjustedPos = new Vector3(transform.position.x, transform.position.y - .5f, transform.position.z);
		if (buttonText) {
			buttonText.transform.position = Camera.main.WorldToViewportPoint(adjustedPos);
		}
	}

	void OnMouseDown() {
		string[] gridCoord;

		// If the button is disabled, do nothing.
		if (disabled) {
			Debug.Log ("This button is disabled");
			return;
		}

		bool newConstruction = false;

		switch(buttonType){
		case "Turret":
			newConstruction = true;
			// Create turret at the grid position this button corresponds to.
			building = (GameObject)Instantiate (Turret, gridPos.transform.position, Quaternion.identity);

			building.name = "Turret-" + gridPos.name;

			// Tells the pathfinding grid to create an obstruction here.
			gridCoord = gridPos.name.Split (',');
			gameManager.addWall (int.Parse(gridCoord[0]),int.Parse(gridCoord[1]));
			building.GetComponent<Turret>().AddGridPosition(gridCoord);
			break;
		case "Trap":
			newConstruction = true;
			// Create trap at the grid position this button corresponds to.
			Vector3 adjustedPos = new Vector3(gridPos.transform.position.x, gridPos.transform.position.y - .05f, gridPos.transform.position.z);
			building = (GameObject)Instantiate (Trap, adjustedPos, Quaternion.identity);			
			building.name = "Trap-" + gridPos.name;
					
			gridCoord = gridPos.name.Split (',');
			building.GetComponent<Trap>().AddGridPosition(gridCoord);
			break;
		case "Wall":
			newConstruction = true;
			// Create wall at the grid position this button corresponds to.
			building = (GameObject)Instantiate (Wall, gridPos.transform.position, Quaternion.identity);
			building.name = "Wall-" + gridPos.name;

			// Tells the pathfinding grid to create an obstruction here.
			gridCoord = gridPos.name.Split (',');
			gameManager.addWall (int.Parse(gridCoord[0]),int.Parse(gridCoord[1]));
			break;
		case "Upgrade":
			UpgradeBuilding();
			break;
		case "Salvage":
			gameManager.EarnMoney (salvageAmount);
			Destroy (building);
			break;
		}

		if (newConstruction ) {
			building.transform.parent = GameObject.Find("Player Buildings").transform;
			building.GetComponent<PlayerBuilding>().lastCost = cost;
			building.GetComponent<PlayerBuilding>().totalCost = cost;
		}
		// Spend player money equal to cost
		gameManager.SpendMoney (cost);
		// Tells the gameManagers pathfinder to find a new path and set it as the master path (to be distributed to enemies)
		masterPathfinder.SetMasterPath (gameManager.grid);
		// Tell the parent radial menu that this space has a building, which means it needs to launch a different menu on click
		//parentMenu.buildType = buttonType;

		// Clear current active menu
		gameManager.SetActiveMenu(null);
	}

	void UpgradeBuilding() {
		building.GetComponent<PlayerBuilding>().totalCost += cost;
		building.GetComponent<PlayerBuilding>().lastCost = cost;

		string type = building.name.Split ('-')[0];
		switch(type) {
		case "Turret":
			building.GetComponent<Turret>().stats.damage = Mathf.RoundToInt (building.GetComponent<Turret>().stats.damage * 1.2f);
			break;
		case "Trap":
			float slowFactor = building.GetComponent<Trap>().slowFactor;
			building.GetComponent<Trap>().slowFactor -= .05f;
			break;
		}
	}
}
